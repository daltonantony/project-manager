# PROJECT MANAGER SERVICE

The back-end service implementation of Project Manager application.

This is a Java Spring Boot application with MySql DB connection.


## Steps to run the app 

- Start the local MySql instance
- Run "mvn spring-boot:run"
- The api can be accessed via http://localhost:8080/
- Projects: http://localhost:8080/projects
- Tasks: http://localhost:8080/alltasks
- Users: http://localhost:8080/users
