package com.dalt;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProjectManagerApplicationTest {

	@Test
	public void contextLoads() {
		// No explicit assertions needed now. Below assertion is to satisfy Sonar.
		Assert.assertTrue(true);
	}

}
