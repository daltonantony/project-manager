package com.dalt.util;

import static java.nio.charset.Charset.forName;
import static org.springframework.http.MediaType.APPLICATION_JSON;

import java.io.IOException;

import org.springframework.http.MediaType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TestUtil {

	public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(APPLICATION_JSON.getType(),
			APPLICATION_JSON.getSubtype(), forName("utf8"));
	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	public static byte[] convertObjectToJsonBytes(final Object object) throws IOException {
		OBJECT_MAPPER.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		return OBJECT_MAPPER.writeValueAsBytes(object);
	}

}