package com.dalt.controllers;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.parseMediaType;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.dalt.model.User;
import com.dalt.service.UserService;
import com.dalt.util.TestUtil;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@WebMvcTest(UserController.class)
public class UserControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private UserService service;

	@Test
	public void findAllUsers() throws Exception {
		final User user = new User();

		when(service.findAllUsers()).thenReturn(asList(user));

		mockMvc.perform(get("/users").accept(parseMediaType("application/json;charset=UTF-8")))
		.andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(1)));

		verify(service, times(1)).findAllUsers();
		verifyNoMoreInteractions(service);
	}

	@Test
	public void findByEmployeeId() throws Exception {
		final User user = new User();

		when(service.findByEmployeeId(1)).thenReturn(user);

		mockMvc.perform(get("/users/1").accept(parseMediaType("application/json;charset=UTF-8")))
		.andExpect(status().isOk());

		verify(service, times(1)).findByEmployeeId(1);
		verifyNoMoreInteractions(service);
	}

	@Test
	public void findUserByProjectId() throws Exception {
		final User user = new User();

		when(service.findUserByProject(1)).thenReturn(user);

		mockMvc.perform(get("/users/projects/1").accept(parseMediaType("application/json;charset=UTF-8")))
		.andExpect(status().isOk());

		verify(service, times(1)).findUserByProject(1);
		verifyNoMoreInteractions(service);
	}

	@Test
	public void findUserByTaskId() throws Exception {
		final User user = new User();

		when(service.findUserByTask(1)).thenReturn(user);
		mockMvc.perform(get("/users/tasks/1").accept(parseMediaType("application/json;charset=UTF-8")))
		.andExpect(status().isOk());

		verify(service, times(1)).findUserByTask(1);
		verifyNoMoreInteractions(service);
	}

	@Test
	public void addUser() throws Exception {
		final User user = new User();

		doNothing().when(service).addUser(user);
		mockMvc.perform(post("/users").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(user))
				.accept(parseMediaType("application/json;charset=UTF-8"))).andExpect(status().isOk());
		final ArgumentCaptor<User> userCapture = ArgumentCaptor.forClass(User.class);
		verify(service, times(1)).addUser(userCapture.capture());
		verifyNoMoreInteractions(service);
	}

	@Test
	public void updateUser() throws Exception {
		final User user = new User();

		doNothing().when(service).updateUser(user);
		mockMvc.perform(put("/users").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(user))
				.accept(parseMediaType("application/json;charset=UTF-8"))).andExpect(status().isOk());
		final ArgumentCaptor<User> userCapture = ArgumentCaptor.forClass(User.class);
		verify(service, times(1)).updateUser(userCapture.capture());
		verifyNoMoreInteractions(service);
	}

	@Test
	public void deleteUser() throws Exception {
		doNothing().when(service).deleteUser(1);
		mockMvc.perform(delete("/users/1").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.accept(parseMediaType("application/json;charset=UTF-8"))).andExpect(status().isOk());
		verify(service, times(1)).deleteUser(1);
		verifyNoMoreInteractions(service);
	}
}
