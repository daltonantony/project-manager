package com.dalt.controllers;

import static com.dalt.util.TestUtil.APPLICATION_JSON_UTF8;
import static com.dalt.util.TestUtil.convertObjectToJsonBytes;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.dalt.model.Task;
import com.dalt.service.TaskService;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@WebMvcTest(TaskController.class)
public class TaskControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private TaskService service;

	@Test
	public void findAllTasks() throws Exception {
		final Task task = new Task();

		when(service.findAllTasks()).thenReturn(asList(task));

		mockMvc.perform(get("/alltasks").accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
		.andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(1)));

		verify(service, times(1)).findAllTasks();
		verifyNoMoreInteractions(service);
	}

	@Test
	public void findTaskById() throws Exception {
		final Task task = new Task();

		when(service.findById(1)).thenReturn(task);

		mockMvc.perform(get("/task/1").accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
		.andExpect(status().isOk());

		verify(service, times(1)).findById(1);
		verifyNoMoreInteractions(service);
	}

	@Test
	public void findTaskByProject() throws Exception {
		final Task task = new Task();
		when(service.findTaskByProject(1)).thenReturn(asList(task));
		mockMvc.perform(get("/task/project/1").accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
		.andExpect(status().isOk());

		verify(service, times(1)).findTaskByProject(1);
		verifyNoMoreInteractions(service);
	}

	@Test
	public void addTask() throws Exception {
		final Task task = new Task();
		doNothing().when(service).addTask(task);
		mockMvc.perform(post("/task").contentType(APPLICATION_JSON_UTF8).content(convertObjectToJsonBytes(task))
				.accept(MediaType.parseMediaType("application/json;charset=UTF-8"))).andExpect(status().isOk());
		final ArgumentCaptor<Task> taskCapture = ArgumentCaptor.forClass(Task.class);
		verify(service, times(1)).addTask(taskCapture.capture());
		verifyNoMoreInteractions(service);
	}

	@Test
	public void updateTask() throws Exception {
		final Task task = new Task();
		doNothing().when(service).updateTask(task);
		mockMvc.perform(put("/task/1").contentType(APPLICATION_JSON_UTF8).content(convertObjectToJsonBytes(task))
				.accept(MediaType.parseMediaType("application/json;charset=UTF-8"))).andExpect(status().isOk());
		final ArgumentCaptor<Task> taskCapture = ArgumentCaptor.forClass(Task.class);
		verify(service, times(1)).updateTask(taskCapture.capture());
		verifyNoMoreInteractions(service);
	}

	@Test
	public void deleteTask() throws Exception {
		doNothing().when(service).deleteTask(1);
		mockMvc.perform(delete("/task/1").contentType(APPLICATION_JSON_UTF8)
				.accept(MediaType.parseMediaType("application/json;charset=UTF-8"))).andExpect(status().isOk());
		verify(service, times(1)).deleteTask(1);
		verifyNoMoreInteractions(service);
	}

	@Test
	public void endTask() throws Exception {
		doNothing().when(service).endTask(1);
		mockMvc.perform(put("/endtask/1").contentType(APPLICATION_JSON_UTF8)
				.accept(MediaType.parseMediaType("application/json;charset=UTF-8"))).andExpect(status().isOk());
		verify(service, times(1)).endTask(1);
		verifyNoMoreInteractions(service);
	}

}
