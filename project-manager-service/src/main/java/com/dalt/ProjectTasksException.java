package com.dalt;

public class ProjectTasksException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ProjectTasksException(final String message) {
		super(message);
	}

}
