package com.dalt.controllers;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dalt.model.Task;
import com.dalt.service.TaskService;

@CrossOrigin
@RestController
public class TaskController {

	@Resource
	private TaskService taskService;

	@GetMapping("/alltasks")
	public List<Task> findAllTasks() {
		return taskService.findAllTasks();
	}

	@GetMapping("/task/{id}")
	public Task findTask(@PathVariable(value = "id") final Long id) {
		return taskService.findById(id);
	}

	@PutMapping("/task/{id}")
	public void updateTask(@PathVariable(value = "id") final Long id, @RequestBody final Task task) {
		task.setId(id);
		taskService.updateTask(task);
	}

	@DeleteMapping("/task/{id}")
	public void deleteTask(@PathVariable(value = "id") final Long id) {
		taskService.deleteTask(id);
	}

	@PostMapping("/task")
	public void addTask(@RequestBody final Task task) {
		taskService.addTask(task);
	}

	@GetMapping(path = "/task/project/{id}")
	public List<Task> findTaskByProject(@PathVariable final Long id) {
		return taskService.findTaskByProject(id);
	}

	@PutMapping(path = "/endtask/{id}")
	public void endTask(@PathVariable final Long id) {
		taskService.endTask(id);
	}
}
