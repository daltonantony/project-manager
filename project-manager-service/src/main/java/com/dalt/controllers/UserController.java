package com.dalt.controllers;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dalt.model.User;
import com.dalt.service.UserService;

@CrossOrigin
@RestController
public class UserController {

	@Resource
	private UserService userService;

	@GetMapping(path = "/users")
	public List<User> findAllUsers() {
		return userService.findAllUsers();
	}

	@GetMapping(path = "/users/{empId}")
	public User findUser(@PathVariable final Long empId) {
		return userService.findByEmployeeId(empId);
	}

	@GetMapping(path = "/users/projects/{id}")
	public User findUserByProjectId(@PathVariable final Long id) {
		return userService.findUserByProject(id);
	}

	@GetMapping(path = "/users/tasks/{id}")
	public User findUserByTaskId(@PathVariable final Long id) {
		return userService.findUserByTask(id);
	}

	@PostMapping(path = "/users")
	public void addUser(@RequestBody final User user) {
		userService.addUser(user);
	}

	@PutMapping(path = "/users")
	public void updateUser(@RequestBody final User user) {
		userService.updateUser(user);
	}

	@DeleteMapping(path = "/users/{empId}")
	public void deleteUser(@PathVariable final Long empId) {
		userService.deleteUser(empId);
	}

}
