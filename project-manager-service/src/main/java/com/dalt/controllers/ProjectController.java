package com.dalt.controllers;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dalt.model.Project;
import com.dalt.service.ProjectService;

@CrossOrigin
@RestController
public class ProjectController {

	@Resource
	private ProjectService projectService;

	@GetMapping(path = "/projects")
	public List<Project> findAllProjects() {
		return projectService.findAllProjects();
	}

	@GetMapping(path = "/projects/tasks")
	public List<Project> findAllProjectsWithTasksCount() {
		return projectService.findAllProjectsWithTasksCount();
	}

	@GetMapping(path = "/projects/{id}")
	public Project findProject(@PathVariable final Long id) {
		return projectService.findById(id);
	}

	@PostMapping(path = "/projects")
	public void addProject(@RequestBody final Project project) {
		projectService.addProject(project);
	}

	@PutMapping(path = "/projects")
	public void updateProject(@RequestBody final Project project) {
		projectService.updateProject(project);
	}

	@DeleteMapping(path = "/projects/{id}")
	public void endProject(@PathVariable final Long id) {
		projectService.endProject(id);
	}
}
