package com.dalt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dalt.model.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {

}
