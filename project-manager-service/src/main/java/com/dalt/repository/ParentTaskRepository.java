package com.dalt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dalt.model.ParentTask;

@Repository
public interface ParentTaskRepository extends JpaRepository<ParentTask, Long> {

}
