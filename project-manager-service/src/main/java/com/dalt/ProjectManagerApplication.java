package com.dalt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectManagerApplication {

	public static void main(final String[] args) {
		SpringApplication.run(ProjectManagerApplication.class, args);
	}

}
